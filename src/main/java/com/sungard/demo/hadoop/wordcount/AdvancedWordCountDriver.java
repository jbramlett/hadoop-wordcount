/*
 * @(#)AdvancedWordCountDriver.java
 *
 * Copyright 2013 Sungard and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or 
 * without modification, are permitted provided that the following 
 * conditions are met:
 * 
 * -Redistributions of source code must retain the above copyright  
 * notice, this  list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright 
 * notice, this list of conditions and the following disclaimer in 
 * the documentation and/or other materials provided with the 
 * distribution.
 * 
 * Neither the name of Sungard and/or its affiliates. or the names of 
 * contributors may be used to endorse or promote products derived 
 * from this software without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any 
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND 
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY 
 * EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY 
 * DAMAGES OR LIABILITIES  SUFFERED BY LICENSEE AS A RESULT OF  OR 
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR 
 * ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE 
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, 
 * SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER 
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF 
 * THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or 
 * intended for use in the design, construction, operation or 
 * maintenance of any nuclear facility. 
 */
package com.sungard.demo.hadoop.wordcount;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

public class AdvancedWordCountDriver {
	private static final Log logger = LogFactory.getLog(AdvancedWordCountDriver.class);

	protected Path localInputPath = new Path("./src/main/resources/gutenberg");
    protected Path hdfsInputPath = new Path("/user/sungard/gutenberg");
    protected Path outputPath = new Path("/user/sungard/output");
	protected Configuration conf;
		
	/**
	 * Constructor
	 */
	public AdvancedWordCountDriver() {
	}

    /**
     * Constructor - provided for testing purposes
     * @param inputPath The input path for our data
     * @param outputPath The output path for our data
     */
    public AdvancedWordCountDriver(Path localInputPath, Path hdfsInputPath, Path outputPath) {
        this.localInputPath = localInputPath;
    	this.hdfsInputPath = hdfsInputPath;
        this.outputPath = outputPath;
    }

	
	/**
	 * Runs our job in a regular fashion
	 * @return int The result of our run
	 */
	public int runJob() {
	    try {
			conf = new Configuration();

			Job job = new Job(conf, "Advanced Word Count");
		    
		    job.setJarByClass(AdvancedWordCountDriver.class);
		    job.setMapperClass(WordCountMapper.class);
		    job.setCombinerClass(WordCountReducer.class);
		    job.setReducerClass(WordCountReducer.class);
		    
		    job.setOutputKeyClass(Text.class);
		    job.setOutputValueClass(IntWritable.class);
		    
			Path inputPath = prepareInputPath();
			Path outputPath = prepareOutputPath();
					    
	    	FileInputFormat.addInputPath(job, inputPath);
	    	FileOutputFormat.setOutputPath(job, outputPath);
	    
	    	return job.waitForCompletion(true) ? 0 : 1;
	    }
	    catch (Exception ioe) {
	    	ioe.printStackTrace();
	    }
	    
	    return 0;
	}
	
	/**
	 * Prepares our input path - if the input data is not available in HDFS we will
	 * put it there by copying from the local filesystem
	 * @return Path the input path
	 */
	protected Path prepareInputPath() throws Exception {
    	FileSystem localFS = FileSystem.getLocal(conf);
    	FileSystem hdfsFS = FileSystem.get(conf);

    	// if our input path does not exist then create it
	    if (!hdfsFS.exists(hdfsInputPath)) {
	    	logger.info("Failed to find our input data - will load from local filesystem");
	    	
	    	FileStatus[] files = localFS.listStatus(localInputPath);
	    	for (FileStatus fs : files) {
	    		if (!fs.isDir()) {
	    			Path hdfsOutput = new Path(hdfsInputPath.toString() + Path.SEPARATOR + fs.getPath().getName());
	    			logger.info("Copying " + hdfsOutput.toString() + " to HDFS");
	    			hdfsFS.copyFromLocalFile(fs.getPath(), hdfsOutput);
	    		}
	    	}
	    	localFS.close();
	    }
		    
	    // since we are running on HDFS make sure to set our input
	    // and output path accordingly
	    return hdfsInputPath;
	}
	
	/**
	 * Prepares our output path
	 * @return Path the output path
	 */
	protected Path prepareOutputPath() throws Exception {
	    // now remove our output directory if it exists
	    FileSystem hdfsFS = outputPath.getFileSystem(conf);
	    if (hdfsFS.exists(outputPath)) {
	    	logger.info("Output path exists - deleting");
	    	hdfsFS.delete(outputPath, true);
	    }
	    
	    return outputPath;
		
	}

	/**
	 * Our main
	 * @param args
	 */
	public static void main(String[] args) {
		AdvancedWordCountDriver driver = new AdvancedWordCountDriver();
		driver.runJob();
	}

}
