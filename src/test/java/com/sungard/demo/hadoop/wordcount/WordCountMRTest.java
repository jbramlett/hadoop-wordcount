/*
 * @(#)WordCountMRTest.java
 *
 * Copyright 2013 Sungard and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or 
 * without modification, are permitted provided that the following 
 * conditions are met:
 * 
 * -Redistributions of source code must retain the above copyright  
 * notice, this  list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright 
 * notice, this list of conditions and the following disclaimer in 
 * the documentation and/or other materials provided with the 
 * distribution.
 * 
 * Neither the name of Sungard and/or its affiliates. or the names of 
 * contributors may be used to endorse or promote products derived 
 * from this software without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any 
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND 
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY 
 * EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY 
 * DAMAGES OR LIABILITIES  SUFFERED BY LICENSEE AS A RESULT OF  OR 
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR 
 * ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE 
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, 
 * SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER 
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF 
 * THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or 
 * intended for use in the design, construction, operation or 
 * maintenance of any nuclear facility. 
 */

package com.sungard.demo.hadoop.wordcount;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

/**
 * Test case for our WordCount Map/Reduce job
 * @author sungard
 */
public class WordCountMRTest extends TestCase {

	  MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;
	  ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;
	  MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;

	  /**
	   * Perform test set-up
	   */
	  @Before
	  public void setUp() {
	    WordCountMapper mapper = new WordCountMapper();
	    WordCountReducer reducer = new WordCountReducer();
	    
	    mapDriver = MapDriver.newMapDriver(mapper);;
	    reduceDriver = ReduceDriver.newReduceDriver(reducer);
	    mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
	  }

	  /**
	   * Tests our mapper
	   */
	  @Test
	  public void testMapper() {
	    mapDriver.withInput(new LongWritable(1), new Text("the quick brown fox"));
	    mapDriver.addOutput(new Text("the"), new IntWritable(1));
	    mapDriver.addOutput(new Text("quick"), new IntWritable(1));
	    mapDriver.addOutput(new Text("brown"), new IntWritable(1));
	    mapDriver.addOutput(new Text("fox"), new IntWritable(1));
	    mapDriver.runTest(false);
	  }

	  /**
	   * Tests our reducer
	   */
	  @Test
	  public void testReducer() {
	    List<IntWritable> values = new ArrayList<IntWritable>();
	    values.add(new IntWritable(1));
	    values.add(new IntWritable(1));
	    reduceDriver.withInput(new Text("the"), values);
	    reduceDriver.withOutput(new Text("the"), new IntWritable(2));
	    reduceDriver.runTest(false);
	  }
	  
	  /**
	   * Tests the map reduce
	   */
	  @Test
	  public void testMapReduce() {
		    mapReduceDriver.addInput(new LongWritable(1), new Text("the quick brown fox the"));
		    mapReduceDriver.addOutput(new Text("the"), new IntWritable(2));
		    mapReduceDriver.addOutput(new Text("quick"), new IntWritable(1));
		    mapReduceDriver.addOutput(new Text("brown"), new IntWritable(1));
		    mapReduceDriver.addOutput(new Text("fox"), new IntWritable(1));
		    
		    mapReduceDriver.runTest(false);
	  }
}
