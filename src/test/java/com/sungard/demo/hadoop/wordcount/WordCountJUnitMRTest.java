/*
 * @(#)WordCountJUnitMRTest.java
 *
 * Copyright 2013 Sungard and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or 
 * without modification, are permitted provided that the following 
 * conditions are met:
 * 
 * -Redistributions of source code must retain the above copyright  
 * notice, this  list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright 
 * notice, this list of conditions and the following disclaimer in 
 * the documentation and/or other materials provided with the 
 * distribution.
 * 
 * Neither the name of Sungard and/or its affiliates. or the names of 
 * contributors may be used to endorse or promote products derived 
 * from this software without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any 
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND 
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY 
 * EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY 
 * DAMAGES OR LIABILITIES  SUFFERED BY LICENSEE AS A RESULT OF  OR 
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR 
 * ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE 
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, 
 * SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER 
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF 
 * THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or 
 * intended for use in the design, construction, operation or 
 * maintenance of any nuclear facility. 
 */
package com.sungard.demo.hadoop.wordcount;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

/**
 * Local test case using local files versus DFS
 * @author sungard
 */
public class WordCountJUnitMRTest extends TestCase {
	private static final Log logger = LogFactory.getLog(WordCountJUnitMRTest.class);
	
	/**
	 * Constructor
	 */
	public WordCountJUnitMRTest() {
		super();
	}

	/**
	 * Constructor
	 * @param name
	 */
	public WordCountJUnitMRTest(String name) {
		super(name);
	}

	/**
	 * Runs a simple test 
	 */
	public void testSimpleMR() throws Exception {
        Configuration config = new Configuration();

        Path inputPath = new Path("./src/test/resources/input");
	    Path outputPath = new Path("./src/test/resources/output");
	    if (outputPath.getFileSystem(config).exists(outputPath)) {
	        outputPath.getFileSystem(config).delete(outputPath, true);
	    }
	    WordCountDriver driver = new WordCountDriver(inputPath, outputPath);
	    
		assertTrue("Failed during map reduce job!", driver.runJob() == 0);
		
		// we ran our test so now verify our results
		FileSystem fs = inputPath.getFileSystem(config);
		FileStatus[] fss = fs.listStatus(outputPath);
		
		Map<String, Integer> wordCounts = new HashMap<String, Integer>();		
		for (FileStatus status : fss) {
			Path path = status.getPath();
			if (path.getName().startsWith("part-r")) {
			    wordCounts.putAll(readResults(path, fs));
			}
		}
		
		// get our expected results
		Map<String, Integer> expectedCounts = readResults(new Path("./src/test/resources/expected_result/sample_data_expected_results.txt"), fs);

		// now verify our results
		assertEquals("Expected number of counts don't match", expectedCounts.size(), wordCounts.size());
		for (String word : wordCounts.keySet()) {
		    Integer count = wordCounts.get(word);
		    Integer expectedCount = expectedCounts.get(word);
		    assertNotNull("Failed to find matching word in expected counts for " + word, expectedCount);
		    assertEquals("Word count doesn't match", expectedCount, count);
		}
	}
	
	/**
	 * Reads an output file creating a map from our word to its count
	 * @param path The path we are loading
	 * @param fs The filesystem used to load this path
	 * @return Map<String, Integer> Maps a word to its count
	 */
	protected Map<String, Integer> readResults(Path path, FileSystem fs) throws Exception {
        Map<String, Integer> wordCounts = new HashMap<String, Integer>();
        
        BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(path)));
        String line = br.readLine();
        while (line != null) {
            String[] lineElements = line.split("\t", 2);
            wordCounts.put(lineElements[0], Integer.valueOf(lineElements[1]));
            line = br.readLine();
        }
        br.close();
        
        return wordCounts;
	}
}